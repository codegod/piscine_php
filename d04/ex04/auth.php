<?php
function auth($login, $passwd)
{
    session_start();
    $passwd = hash("whirlpool", $passwd);
    $file = "../private/passwd";
    $users = unserialize(file_get_contents($file));
    $ok = 0;
    foreach($users as $id => $user)
    {
        if ($user['login'] === $login && $user['passwd'] === $passwd)
        {
            $_SESSION['loggued_on_user'] = $login;
            return (true);
        }
    }
    $_SESSION['loggued_on_user'] = "";
    return (false);
}
?>