<?php
    if ($_POST['login'] && $_POST['passwd'] && $_POST['submit'] && $_POST['submit'] === "OK")
    {
        if (!file_exists("../private"))
            mkdir("../private");
        if (!file_exists("../private/passwd"))
            file_put_contents("../private/passwd", null);
        $acc = unserialize(file_get_contents('../private/passwd'));
        if ($acc)
            foreach ($acc as $ac => $info)
                if ($info['login'] === $_POST['login'])
                    $exist = 1;
        if (!$exist)
        {
            $new['login'] = $_POST['login'];
            $new['passwd'] = hash('whirlpool', $_POST['passwd']);
            $acc[] = $new;
            file_put_contents('../private/passwd', serialize($acc));
            echo "OK\n";
            header('Location: index.html');
        }
        else
            echo "ERROR\n";
    }
    else
        echo "ERROR\n";
?>