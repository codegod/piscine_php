<?php
    if ($_POST['login'] && $_POST['oldpw'] && $_POST['newpw'] && $_POST['submit'] && $_POST['submit'] === "OK")
    {
        $acc = unserialize(file_get_contents('../private/passwd'));
        if ($acc)
            foreach ($acc as $ac => $info)
            if ($info['login'] === $_POST['login'] && $info['passwd'] === hash('whirlpool', $_POST['oldpw']))
            {
                $exist = 1;
                $acc[$ac]['passwd'] =  hash('whirlpool', $_POST['newpw']);
            }
        if ($exist)
        {
            file_put_contents('../private/passwd', serialize($acc));
            echo "OK\n";
            header('Location: index.html');
        }
        else
            echo "ERROR\n";
    }
    else
        echo "ERROR\n";
?>