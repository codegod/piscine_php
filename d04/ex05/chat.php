<?php
    session_start();
    $file = '../private/chat';
    if (!($_SESSION['loggued_on_user']))
        echo "ERROR\n";
    else {
        if (file_exists($file)) {
            $chat = unserialize(file_get_contents($file));
            if ($chat)
                foreach ($chat as $v) {
                    echo "[" . date('H:i', $v['time']) . "] <b>" . $v['login'] . "</b>: " . $v['msg'] . "<br />\n";
                }
        }
    }
?>