<?php
    if ($_POST['login'] != "" && $_POST['passwd'] != "" && $_POST['submit'] === "OK")
    {
        $pass = hash("whirlpool", $_POST['passwd']);
        $file = "../private/passwd";
        if (file_exists($file))
        {
            $ok = 1;
            $str = file_get_contents($file);
            $un = unserialize($str);
            foreach ($un as $key => $val)
            {
                $con = $key;
                if ($val['login'] == $_POST['login'])
                    $ok = 0;
            }
            if ($ok)
            {
                $un[$con + 1] = array('login' => $_POST['login'], 'passwd' => $pass);
                file_put_contents($file, serialize($un));
                echo "OK\n";
            }
            else
            {
                echo "ERROR\n";
            }
        }
        else
        {
            mkdir("../private/");
            $cred[0]['login'] = $_POST['login'];
            $cred[0]['passwd'] = $pass;
            file_put_contents($file, serialize($cred));
            echo "OK\n";
        }
    }
    else
    {
        echo "ERROR\n";
    }
?>