<?php
    if ($_POST['login'] != "" && $_POST['oldpw'] != "" && $_POST['newpw'] && $_POST['submit'] === "OK")
    {
        $oldp = hash("whirlpool", $_POST['oldpw']);
        $newp = hash("whirlpool", $_POST['newpw']);
        $file = "../private/passwd";
        $users = unserialize(file_get_contents($file));
        $ok = 0;
        foreach($users as $id => $user)
        {
            if ($user['login'] === $_POST['login'] && $user['passwd'] === $oldp)
            {
                $users[$id]['passwd'] = $newp;
                file_put_contents($file, serialize($users));
                echo "OK\n";
                $ok = 1;
            }
        }
        if (!$ok)
            echo "ERROR\n";;
    }
    else
    {
        echo "ERROR\n";
    }
?>