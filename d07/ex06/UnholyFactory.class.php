<?php
require_once('Fighter.class.php');

class UnholyFactory {

        private $fList;

        public function absorb($newF) {
            if (get_parent_class($newF) == "Fighter") {
                if (isset($this->fList[$newF()]))
                    print("(Factory already absorbed a fighter of type " . $newF() . ")" . PHP_EOL);
                else
                {
                    print("(Factory absorbed a fighter of type " . $newF() . ")" . PHP_EOL);
                    $this->fList[$newF()] = $newF;
                }
            }
            else
                print("(Factory can't absorb this, it's not a fighter)" . PHP_EOL);
        }

        public function fabricate($fighter) {
            if (array_key_exists($fighter, $this->fList)) {
                print("(Factory fabricates a fighter of type " . $fighter . ")" . PHP_EOL);
                return (clone $this->fList[$fighter]);
            }
            print("(Factory hasn't absorbed any fighter of type " . $fighter . ")" . PHP_EOL);
            return null;
        }
}
?>