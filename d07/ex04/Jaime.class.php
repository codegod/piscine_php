<?php
Class Jaime {
    public function sleepWith( $rhs ) {
        if ($rhs instanceof Tyrion)
            print("Not even if I'm drunk !" . PHP_EOL);
        elseif ($rhs instanceof Lannister)
            print("With pleasure, but only in a tower in Winterfell, then." . PHP_EOL);
        else
            print("Let's do this." . PHP_EOL);
    }
}
?>