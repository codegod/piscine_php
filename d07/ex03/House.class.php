<?php
Class House {
	public function __toString() {
	    return (sprintf("House %s of %s : \"%s\"", $this->getHouseName(), $this->getHouseSeat(), $this->getHouseMotto()));
	}
	public function introduce() {
	    print($this . PHP_EOL);
	}
}
?>