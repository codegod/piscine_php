
<- Vertex ----------------------------------------------------------------------
The Vertex class handles the initialization of a vertex point in spase acording
to five characteristics:
Its x axis coordinate
Its y axis coordinate
Its z depth coordinate
Its w homogeneous coordinates (that can represent points at infinity as finite 
coordinates)
Its color that is type of Color class from ex00
An instanse can be constructed like that
$col = new Color();
new Vertex(array('x'=> 0.0, 'y'=>0.0, 'z'=>0.0, 'w'=>1.0, 'color'=>$col));
The w and color keys are optional and they have the folowing defaults:
w = 1.0
color = new Color('rgb' => 0xFFFFFF ) (white color)
The $verbose variable handles the outputs. default is "false", switch it to 
"true" for more information.
---------------------------------------------------------------------- Vertex ->
