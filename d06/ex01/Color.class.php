<?php
Class Color {
    public $red = 0;
    public $green = 0;
    public $blue = 0;
    static $verbose = False;

    public function __construct( array $kwargs ) {
        if (array_key_exists('rgb', $kwargs)) {
            $tmp = $kwargs['rgb'];
            $this->red = ($tmp >> 16) & 0xFF;
            $this->green = ($tmp >>8 ) & 0xFF;
            $this->blue = ($tmp) & 0xFF;
        }
        elseif (array_key_exists('red', $kwargs) && array_key_exists('green', $kwargs) && array_key_exists('blue', $kwargs)) {
            $this->red = $kwargs['red'];
            $this->green = $kwargs['green'];
            $this->blue = $kwargs['blue'];
        }
        if (self::$verbose)
            print($this . " constructed." . PHP_EOL);
        return;
    }
    function __toString() {
        return sprintf("Color( red: %3d, green: %3d, blue: %3d )", $this->red, $this->green, $this->blue);
    }
    function doc() {
        return (file_get_contents("Color.doc.txt"));
    }
    function add($elem) {
        return (new Color(array ('red' => $elem->red + $this->red, 'green' => $elem->green + $this->green, 'blue' => $elem->blue + $this->blue)));
    }
    function sub($elem) {
        return (new Color(array ('red' => abs($elem->red - $this->red), 'green' => abs($elem->green - $this->green), 'blue' => abs($elem->blue - $this->blue))));
    }
    function mult($elem) {
        return (new Color(array ('red' => $elem * $this->red, 'green' => $elem * $this->green, 'blue' => $elem * $this->blue)));
    }
    public function __destruct() {
        if (self::$verbose)
            print($this . " destructed." . PHP_EOL);
        return;
    }
}
?>