<?php

require_once "Color.class.php";

Class Vertex {
    private $_x;
    private $_y;
    private $_z;
    private $_w = 1.0;
    private $_color;
    static $verbose = False;
    public function __construct( array $args ) {
        if (array_key_exists('x', $args) && array_key_exists('y', $args) && array_key_exists('z', $args)) {
            $this->_x = $args['x'];
            $this->_y = $args['y'];
            $this->_z = $args['z'];
            if (array_key_exists('w', $args)) {
                $this->_w = $args['w'];
            }
            if (array_key_exists('color', $args)) {
                $this->_color = $args['color'];
            }
            else {
                $this->_color = new Color( array( 'red' => 0xff, 'green' => 0xff, 'blue' => 0xff ) );
            }
        }
        if (self::$verbose) {
             print($this . " constructed" . PHP_EOL);
        }
        return;
    }
    function doc() {
        return (file_get_contents("Vertex.doc.txt"));
    }
    function __toString() {
        if (self::$verbose)
            return sprintf("Vertex( x: %3.2f, y: %3.2f, z:%3.2f, w:%3.2f, %s )", $this->_x, $this->_y, $this->_z, $this->_w, $this->_color);
        else
            return sprintf("Vertex( x: %3.2f, y: %3.2f, z:%3.2f, w:%3.2f )", $this->_x, $this->_y, $this->_z, $this->_w);
        
    }
    public function __destruct() {
        if (self::$verbose) {
             print($this . " destructed" . PHP_EOL);
        }
        return;
    }
}
?>