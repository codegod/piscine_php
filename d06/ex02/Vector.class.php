<?php

require_once 'Vertex.class.php';

Class Vector {
    private $_x;
    private $_y;
    private $_z;
    private $_w;
    static $verbose = False;
    public function __construct( array $args ) {
        if (array_key_exists('dest', $args) && array_key_exists('orig', $args)) {
            $this->_x = $args['dest']->getParams()['x'] - $args['orig']->getParams()['x'];
            $this->_y = $args['dest']->getParams()['y'] - $args['orig']->getParams()['y'];
            $this->_z = $args['dest']->getParams()['z'] - $args['orig']->getParams()['z'];
            $this->_w = $args['dest']->getParams()['w'] - $args['orig']->getParams()['w'];
        }
        elseif (array_key_exists('dest', $args) ) {
            $ne = new Vertex( array( 'x' => 0.0, 'y' => 0.0, 'z' => 0.0 ) );
            $this->_x = $args['dest']->getParams()['x'] - $ne->getParams()['x'];
            $this->_y = $args['dest']->getParams()['y'] - $ne->getParams()['y'];
            $this->_z = $args['dest']->getParams()['z'] - $ne->getParams()['z'];
            $this->_w = $args['dest']->getParams()['w'] - $ne->getParams()['w'];
        }
        if (self::$verbose)
            print($this . " constructed" . PHP_EOL);
        return;
    }
    function magnitude() {
        return (pow(pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2), 0.5));
    }
    function normalize() {
        return (new Vector( array('dest' => new Vertex( array( 'x' => $this->_x / $this->magnitude(), 'y' => $this->_y / $this->magnitude(), 'z' => $this->_z / $this->magnitude() ) ) ) ));
    }
    function add( $rhs ) {
        return (new Vector( array('dest' => new Vertex( array( 'x' => $this->_x + $rhs->_x, 'y' => $this->_y + $rhs->_y, 'z' => $this->_z + $rhs->_z ) ) ) ));
    }
    function sub( $rhs ) {
        return (new Vector( array('dest' => new Vertex( array( 'x' => $this->_x + -$rhs->_x, 'y' => $this->_y + -$rhs->_y, 'z' => $this->_z + -$rhs->_z ) ) ) ));
    }
    function opposite() {
        return (new Vector( array('dest' => new Vertex( array( 'x' => -$this->_x, 'y' => -$this->_y, 'z' => -$this->_z ) ) ) ));
    }
    function scalarProduct( $k ) {
        return (new Vector( array('dest' => new Vertex( array( 'x' => $this->_x * $k, 'y' => $this->_y * $k, 'z' => $this->_z * $k ) ) ) ));
    }
    function dotProduct( $rhs ) {
        return ($this->magnitude() * $rhs->magnitude() * $this->cos($rhs));
    }
    function crossProduct( $rhs ) {
        return (new Vector( array('dest' => new Vertex( array('x' => $this->_y * $rhs->_z - $this->_z * $rhs->_y, 'y' => $this->_z * $rhs->_x - $this->_x * $rhs->_z, 'z' => $this->_x * $rhs->_y - $this->_y * $rhs->_x) ) ) ) );
    }
    function cos( $rhs ) {
        $x = $this->_x * $rhs->_x; 
        $y = $this->_y * $rhs->_y;
        $z = $this->_z * $rhs->_z;
        return (($x + $y + $z) / ($this->magnitude() * $rhs->magnitude()));
    }
    function doc() {
        return (file_get_contents("Vector.doc.txt"));
    }
    function __toString() {
            return sprintf("Vector( x:%3.2f, y:%3.2f, z:%3.2f, w:%3.2f )", $this->_x, $this->_y, $this->_z, $this->_w);
    }
    public function __destruct() {
        if (self::$verbose)
            print($this . " destructed" . PHP_EOL);
        return;
    }
}
?>